# Generated by Django 5.0.6 on 2024-06-26 04:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0002_task_company"),
    ]

    operations = [
        migrations.AddField(
            model_name="task",
            name="progress",
            field=models.IntegerField(default=0, editable=False),
        ),
    ]
