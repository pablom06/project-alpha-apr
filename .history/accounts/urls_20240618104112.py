from django.urls import path
from accounts.views import user_login, logout_view, Si
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name='accounts/login.html'), name='login'),
    path('logout/', logout_view, name='logout'),
    path('signup/', signup, name='signup'),
]
