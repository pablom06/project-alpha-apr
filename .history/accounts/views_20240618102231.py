from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from accounts.forms import Login, SignUp
from django.contrib.auth.models import User

def user_login(request):
    if request.method == "POST":
        form = Login(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username = username, password = password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    return render(request, "accounts/login.html")

def logout_view(request):
    logout(request)
    return redirect("login")

def signup(request):
    if request.method == "POST":
        form = SignUp(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirm = form.cleaned_data["password_confirm"]
            if password == password_confirm:
                user = User.objects.create_user(username = username, password = password)
                user.save()
                return redirect("login")
            else:
                if password != password_confirm:
                    form.add_error('password_confirm', 'The passwords do not match')
    return render(request, "accounts/signup.html")
