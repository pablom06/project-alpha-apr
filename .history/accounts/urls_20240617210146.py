from django.urls import path
from accounts.views import user_login
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('login/', user_login, name='login'),
]
