from django.urls import path
from accounts.views import user_login
from django.contrib.auth import views as auth_views

urlpatterns = [
    path(auth_views.LoginView.as_view(template_name='accounts/login.html'), name='login'),
]
