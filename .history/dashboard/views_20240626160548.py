from django.shortcuts import render, get_object_or_404
from tasks.models import Task
from projects.models import Project, Company
from django.utils.timezone import now
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.db.models import Count

@login_required
def dashboard_view(request):
    total_projects = Project.objects.count()
    total_tasks = Task.objects.count()
    total_companies = Company.objects.count()
    active_projects = Project.objects.filter(tasks__is_complete=False).distinct().count()
    completed_projects_ytd = Project.objects.filter(tasks__is_complete=True, tasks__due_date__year=now().year).distinct().count()

    project_id = request.GET.get('project_id')
    companies_by_state = []
    projects = Project.objects.all()
    selected_project = None

    if project_id and project_id != 'all':
        selected_project = get_object_or_404(Project, id=project_id)
        companies_by_state = Company.objects.filter(project=selected_project).values('state').annotate(count=Count('id')).order_by('state')
    else:
        companies_by_state = Company.objects.values('state').annotate(count=Count('id')).order_by('state')

    print(companies_by_state)  # Debugging statement to check data

    tasks = Task.objects.filter(project=selected_project) if selected_project else []
    task_data = [{'id': str(task.id), 'name': task.name, 'start': task.start_date.strftime('%Y-%m-%d'), 'end': task.due_date.strftime('%Y-%m-%d'), 'progress': task.progress} for task in tasks]

    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        data = {
            'total_projects': total_projects,
            'total_tasks': total_tasks,
            'total_companies': total_companies,
            'active_projects': active_projects,
            'completed_projects_ytd': completed_projects_ytd,
            'task_data': task_data,
            'project_name': selected_project.name if selected_project else '',
            'companies_by_state': list(companies_by_state)
        }
        return JsonResponse(data)

    context = {
        'total_projects': total_projects,
        'total_tasks': total_tasks,
        'total_companies': total_companies,
        'active_projects': active_projects,
        'completed_projects_ytd': completed_projects_ytd,
        'project': selected_project,
        'tasks': task_data,
        'projects': projects,
        'selected_project': selected_project,
        'companies_by_state': companies_by_state,
    }

    return render(request, 'dashboard/dashboard.html', context)
