from django.shortcuts import render, get_object_or_404
from tasks.models import Task
from projects.models import Project, Company
from django.utils.timezone import now
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.db.models import Count
import json

@login_required
def dashboard_view(request):
    project_id = request.GET.get('project_id')

    if project_id and project_id != 'all':
    project = get_object_or_404(Project, id=project_id)
    tasks = Task.objects.filter(project=project)
    project_name = project.name
    total_projects = 1
    total_tasks = tasks.count()
    total_companies = Company.objects.filter(tasks__project=project).distinct().count()
    active_projects = Project.objects.filter(id=project_id, tasks__is_complete=False).distinct().count()
    completed_projects_ytd = Project.objects.filter(id=project_id, tasks__is_complete=True, tasks__due_date__year=now().year).distinct().count()
    companies_by_state = Company.objects.filter(tasks__project=project).values('state').annotate(count=Count('id')).order_by('state')
else:
    tasks = Task.objects.all()
    project_name = "All Projects"
    total_projects = Project.objects.count()
    total_tasks = tasks.count()
    total_companies = Company.objects.filter(tasks__project__in=Project.objects.all()).distinct().count()
    active_projects = Project.objects.filter(tasks__is_complete=False).distinct().count()
    completed_projects_ytd = Project.objects.filter(tasks__is_complete=True, tasks__due_date__year=now().year).distinct().count()
    companies_by_state = Company.objects.filter(tasks__project__in=Project.objects.all()).values('state').annotate(count=Count('id')).order_by('state')


    task_data = []
    for task in tasks:
        task_data.append({
            'id': str(task.id),
            'name': task.name,
            'start': task.start_date.strftime('%Y-%m-%d'),
            'end': task.due_date.strftime('%Y-%m-%d'),
            'progress': task.progress,
            'assignee': task.assignee.username if task.assignee else None,
            'company': task.company.name if task.company else None,
            'project': task.project.name,
        })

    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        return JsonResponse({
            'total_projects': total_projects,
            'total_tasks': total_tasks,
            'total_companies': total_companies,
            'active_projects': active_projects,
            'completed_projects_ytd': completed_projects_ytd,
            'task_data': task_data,
            'project_name': project_name,
            'companies_by_state': list(companies_by_state),
        })

    context = {
        'total_projects': total_projects,
        'total_tasks': total_tasks,
        'total_companies': total_companies,
        'active_projects': active_projects,
        'completed_projects_ytd': completed_projects_ytd,
        'project': project_name,
        'tasks': task_data,
        'projects': Project.objects.all(),
        'companies_by_state_json': json.dumps(list(companies_by_state)),
        'tasks_json': json.dumps(task_data),
        'project_name_json': json.dumps(project_name)
    }

    return render(request, 'dashboard/dashboard.html', context)
