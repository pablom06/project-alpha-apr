from django.shortcuts import render, get_object_or_404
from tasks.models import Task
from projects.models import Project, Company
from django.utils.timezone import now
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.db.models import Count
import json

@login_required
def dashboard_view(request):
    total_projects = Project.objects.count()
    total_tasks = Task.objects.count()
    total_companies = Company.objects.count()
    active_projects = Project.objects.filter(tasks__is_complete=False).distinct().count()
    completed_projects_ytd = Project.objects.filter(tasks__is_complete=True, tasks__due_date__year=now().year).distinct().count()

    project_id = request.GET.get('project_id')

    if project_id and project_id != 'all':
        project = get_object_or_404(Project, id=project_id)
        tasks = Task.objects.filter(project=project)
        project_name = project.name
    else:
        project = None
        tasks = Task.objects.all()
        project_name = "All Projects"

    task_data = []
    for task in tasks:
        task_data.append({
            'id': str(task.id),
            'name': task.name,
            'start': task.start_date.strftime('%Y-%m-%d'),
            'end': task.due_date.strftime('%Y-%m-%d'),
            'progress': task.progress,
            'assignee': task.assignee.username if task.assignee else None,
            'company': task.company.name if task.company else None,
            'project': task.project.name,
        })

    companies_by_state = list(Company.objects.values('state').annotate(count=Count('id')).order_by('state'))

    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        return JsonResponse({
            'total_projects': total_projects,
            'total_tasks': total_tasks,
            'total_companies': total_companies,
            'active_projects': active_projects,
            'completed_projects_ytd': completed_projects_ytd,
            'task_data': task_data,
            'project_name': project_name,
            'companies_by_state': companies_by_state,
        })

    context = {
        'total_projects': total_projects,
        'total_tasks': total_tasks,
        'total_companies': total_companies,
        'active_projects': active_projects,
        'completed_projects_ytd': completed_projects_ytd,
        'project': project,
        'tasks': task_data,
        'projects': Project.objects.all(),
        'companies_by_state_json': json.dumps(companies_by_state),
        'tasks_json': json.dumps(task_data),
        'project_name_json': json.dumps(project_name)
    }

    return render(request, 'dashboard/dashboard.html', context)
