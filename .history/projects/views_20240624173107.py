from django.shortcuts import render, get_object_or_404, redirect
from .models import Project, Company
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm, CompanyForm

@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    context = {"project": project, "tasks": tasks}
    return render(request, "projects/show_project.html", context)

@login_required
def new_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            new_company_name = form.cleaned_data.get("new_company")
            if new_company_name:
                company, created = Company.objects.get_or_create(name=new_company_name)
                form.instance.company = company
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)

@login_required
def edit_project(request, id):
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect('show_project', id=project.id)
    else:
        form = ProjectForm(instance=project)
    context = {"form": form, "project": project}
    return render(request, "projects/edit_project.html", context)

@login_required
def list_companies(request):
    companies = Company.objects.filter(owner=request.user)
    context = {"companies": companies}
    return render(request, "projects/companies.html", context)

@login_required
def add_company(request):
    form = CompanyForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("list_companies")
    context = {"form": form}
    return render(request, "projects/add_company.html", context)

@login_required
def search_projects_by_name(request):
    query = request.GET.get("query")
    projects = Project.objects.filter(name__icontains=query, owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects.html", context)
