from django.db import models

class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey("auth.User", related_name='projects', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_complete = models.BooleanField(default=False)

    def __str__(self):
        return self.name
