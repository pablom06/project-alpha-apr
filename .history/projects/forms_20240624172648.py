from django.forms import forms
from .models import Project, Company

class ProjectForm(forms.ModelForm):
    new_company = forms.CharField(max_length=200, required=False)
    class Meta:
        model = Project
        fields = ["name", "description", "owner", "company"]
        widgets = {
            'company': Select()
        }

class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = ["name", "street_address", "city", "state", "zip_code", "description", "owner"]
