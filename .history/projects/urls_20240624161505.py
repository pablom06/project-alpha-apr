from django.urls import path
from .views import list_projects, show_project, new_project, search_projects_by_name, list_companies

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("create/", new_project, name="create_project"),
    path("<int:id>/", show_project, name="show_project"),
    path("search/", search_projects_by_name, name="search_projects"),
]
