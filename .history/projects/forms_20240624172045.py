from django.forms import ModelForm, Select
from .models import Project, Company

class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner", "company"]
        widgets = {
            'company': forms.Select()
        }

class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = ["name", "street_address", "city", "state", "zip_code", "description", "owner"]
