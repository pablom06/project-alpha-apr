from django.urls import path
from .views import show_projects

urlpatterns = [
    path("", list_projects, name = "list_projects"),
]
