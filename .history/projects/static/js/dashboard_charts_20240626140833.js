function initializeCharts(totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks) {
    // Initialize charts
    var ctxPie = document.getElementById('myPieChart').getContext('2d');
    var pieChart = new Chart(ctxPie, {
        type: 'pie',
        data: {
            labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
            datasets: [{
                data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(153, 102, 255, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
        }
    });

    var ctxBar = document.getElementById('myBarChart').getContext('2d');
    var barChart = new Chart(ctxBar, {
        type: 'bar',
        data: {
            labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
            datasets: [{
                label: 'Count',
                data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(153, 102, 255, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    var ctxLine = document.getElementById('myLineChart').getContext('2d');
    var lineChart = new Chart(ctxLine, {
        type: 'line',
        data: {
            labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
            datasets: [{
                label: 'Count',
                data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                fill: false,
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    // Initial update of task list
    updateTaskList(tasks);

    // Attach charts to window object for further updates
    window.pieChart = pieChart;
    window.barChart = barChart;
    window.lineChart = lineChart;
}

function updateDashboardData(totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks) {
    // Pie chart data
    var pieData = {
        labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
        datasets: [{
            data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
            backgroundColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
                'rgba(75, 192, 192, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    };

    // Bar chart data
    var barData = {
        labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
        datasets: [{
            label: 'Count',
            data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
            backgroundColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
                'rgba(75, 192, 192, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    };

    // Line chart data
    var lineData = {
        labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
        datasets: [{
            label: 'Count',
            data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            borderColor: 'rgba(75, 192, 192, 1)',
            fill: false,
            borderWidth: 1
        }]
    };

    // Update charts
    updateChart(window.pieChart, pieData);
    updateChart(window.barChart, barData);
    updateChart(window.lineChart, lineData);

    // Update task list with progress
    updateTaskList(tasks);
}

function updateChart(chart, data) {
    chart.data = data;
    chart.update();
}

function updateTaskList(tasks) {
    var taskList = document.getElementById('task-list');
    taskList.innerHTML = '';
    tasks.forEach(task => {
        var listItem = document.createElement('li');
        listItem.textContent = `${task.name}: ${task.start} to ${task.end} (Progress: ${task.progress}%)`;
        taskList.appendChild(listItem);
    });
}

