document.addEventListener('DOMContentLoaded', function () {
    var total_projects = parseInt(document.querySelector('.dashboard-cards .total-projects h2').textContent, 10);
    var total_tasks = parseInt(document.querySelector('.dashboard-cards .total-tasks h2').textContent, 10);
    var total_companies = parseInt(document.querySelector('.dashboard-cards .total-companies h2').textContent, 10);
    var active_projects = parseInt(document.querySelector('.dashboard-cards .active-projects h2').textContent, 10);
    var completed_projects_ytd = parseInt(document.querySelector('.dashboard-cards .completed-projects-ytd h2').textContent, 10);

    var pieData = {
        labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
        datasets: [{
            data: [total_projects, total_tasks, total_companies, active_projects, completed_projects_ytd],
            backgroundColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
                'rgba(75, 192, 192, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    };

    var barData = {
        labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
        datasets: [{
            label: 'Count',
            data: [total_projects, total_tasks, total_companies, active_projects, completed_projects_ytd],
            backgroundColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
                'rgba(75, 192, 192, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    };

    var lineData = {
        labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
        datasets: [{
            label: 'Count',
            data: [total_projects, total_tasks, total_companies, active_projects, completed_projects_ytd],
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            borderColor: 'rgba(75, 192, 192, 1)',
            fill: false,
            borderWidth: 1
        }]
    };

    var ctxPie = document.getElementById('myPieChart').getContext('2d');
    new Chart(ctxPie, {
        type: 'pie',
        data: pieData,
        options: {
            responsive: true,
            maintainAspectRatio: false,
        }
    });

    var ctxBar = document.getElementById('myBarChart').getContext('2d');
    new Chart(ctxBar, {
        type: 'bar',
        data: barData,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    var ctxLine = document.getElementById('myLineChart').getContext('2d');
    new Chart(ctxLine, {
        type: 'line',
        data: lineData,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
});
