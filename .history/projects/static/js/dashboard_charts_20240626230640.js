console.log('dashboard_charts.js loaded');

var barChart, lineChart;

function initializeCharts(totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState) {
    console.log('Initializing charts with:', { totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState });

    if (barChart) {
        barChart.destroy();
    }

    if (lineChart) {
        lineChart.destroy();
    }

    document.getElementById('total-projects').textContent = totalProjects;
    document.getElementById('total-tasks').textContent = totalTasks;
    document.getElementById('total-companies').textContent = totalCompanies;
    document.getElementById('active-projects').textContent = activeProjects;
    document.getElementById('completed-projects-ytd').textContent = completedProjectsYtd;

    var ctxBar = document.getElementById('myBarChart').getContext('2d');
    barChart = new Chart(ctxBar, {
        type: 'bar',
        data: {
            labels: companiesByState.map(item => item.state),
            datasets: [{
                label: 'Companies',
                data: companiesByState.map(item => item.count),
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    var ctxLine = document.getElementById('myLineChart').getContext('2d');
    lineChart = new Chart(ctxLine, {
        type: 'line',
        data: {
            labels: tasks.map(task => task.name),
            datasets: [{
                label: 'Task Progress',
                data: tasks.map(task => task.progress),
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                fill: false,
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true,
                    max: 100
                }
            }
        }
    });

    updateTaskList(tasks);
    updateProjectName(projectName);
}

function updateDashboardData(totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState) {
    console.log('Updating dashboard data with:', { totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState });

    initializeCharts(totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState);
}

function updateTaskList(tasks) {
    console.log('Updating task list:', tasks);
    var taskList = document.getElementById('task-list');
    taskList.innerHTML = '';
    tasks.forEach(task => {
        var listItem = document.createElement('li');
        listItem.textContent = `${task.name}: ${task.start} to ${task.end} (Progress: ${task.progress}%)`;
        taskList.appendChild(listItem);
    });
}

function updateProjectName(projectName) {
    console.log('Updating project name to:', projectName);
    document.getElementById('project-name').textContent = projectName;
}

document.addEventListener('DOMContentLoaded', function () {
    console.log('DOM content loaded');

    var dashboardData = JSON.parse(document.getElementById('dashboard-data').textContent);

    var totalProjects = dashboardData.totalProjects;
    var totalTasks = dashboardData.totalTasks;
    var totalCompanies = dashboardData.totalCompanies;
    var activeProjects = dashboardData.activeProjects;
    var completedProjectsYtd = dashboardData.completedProjectsYtd;
    var tasks = dashboardData.tasks;
    var projectName = dashboardData.projectName;
    var companiesByState = dashboardData.companiesByState;

    console.log('Data:', totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState);

    initializeCharts(totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState);

    document.getElementById('project-select').addEventListener('change', function () {
        const projectId = this.value;
        console.log(`Selected project ID: ${projectId}`);

        const csrfToken = document.querySelector('[name=csrfmiddlewaretoken]').value;

        fetch(`/dashboard/?project_id=${projectId}`, {
            method: 'GET',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRFToken': csrfToken
            }
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('Data received:', data);
            updateDashboardData(
                data.total_projects,
                data.total_tasks,
                data.total_companies,
                data.active_projects,
                data.completed_projects_ytd,
                data.task_data,
                data.project_name,
                data.companies_by_state
            );
        })
        .catch(error => console.error('Error:', error));
    });
});

