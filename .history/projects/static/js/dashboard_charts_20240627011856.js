console.log('dashboard_charts.js loaded');

var pieChart, ganttChart;

function initializeCharts(companiesByState, tasks) {
    console.log('Initializing charts with:', { companiesByState, tasks });

    if (pieChart) {
        pieChart.destroy();
    }

    if (ganttChart) {
        ganttChart.destroy();
    }

    // Pie Chart for Companies by State
    var companiesCtx = document.getElementById('myPieChart').getContext('2d');
    var companiesLabels = companiesByState.map(data => data.state);
    var companiesData = companiesByState.map(data => data.count);
    var totalCompanies = companiesData.reduce((a, b) => a + b, 0);
    var companiesPercentages = companiesData.map(count => ((count / totalCompanies) * 100).toFixed(2));

    pieChart = new Chart(companiesCtx, {
        type: 'pie',
        data: {
            labels: companiesLabels.map((label, index) => `${label} (${companiesPercentages[index]}%)`),
            datasets: [{
                data: companiesPercentages,
                backgroundColor: companiesLabels.map((_, index) => `hsl(${index * 30}, 70%, 50%)`), // Generate different colors
                borderColor: '#fff',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(context) {
                            var label = context.label || '';
                            var value = context.raw || 0;
                            return `${label}: ${value}%`;
                        }
                    }
                }
            }
        }
    });

    // Gantt Chart for Task Progress
    var tasksCtx = document.getElementById('myGanttChart').getContext('2d');
    var ganttData = tasks.map(task => ({
        x: new Date(task.start),
        x2: new Date(task.end),
        y: task.name,
        progress: task.progress
    }));

    ganttChart = new Chart(tasksCtx, {
        type: 'bar',
        data: {
            datasets: [{
                label: 'Task Progress',
                data: ganttData,
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                x: {
                    type: 'time',
                    time: {
                        unit: 'day'
                    },
                    position: 'bottom'
                },
                y: {
                    type: 'category',
                    labels: tasks.map(task => task.name)
                }
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(context) {
                            const task = context.raw;
                            return `${task.y}: ${task.progress}%`;
                        }
                    }
                }
            }
        }
    });

    updateTaskList(tasks);
    updateProjectName(tasks[0]?.project || 'All Projects');
}

function updateDashboardData(totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState) {
    console.log('Updating dashboard data with:', { totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState });

    document.getElementById('total-projects').textContent = totalProjects;
    document.getElementById('total-tasks').textContent = totalTasks;
    document.getElementById('total-companies').textContent = totalCompanies;
    document.getElementById('active-projects').textContent = activeProjects;
    document.getElementById('completed-projects-ytd').textContent = completedProjectsYtd;

    initializeCharts(companiesByState, tasks);
}

function updateTaskList(tasks) {
    console.log('Updating task list:', tasks);
    var taskList = document.getElementById('task-list');
    taskList.innerHTML = '';
    tasks.forEach(task => {
        var listItem = document.createElement('li');
        listItem.textContent = `${task.name}: ${task.start} to ${task.end} (Progress: ${task.progress}%)`;
        taskList.appendChild(listItem);
    });
}

function updateProjectName(projectName) {
    console.log('Updating project name to:', projectName);
    document.getElementById('project-name').textContent = projectName;
}

document.addEventListener('DOMContentLoaded', function () {
    console.log('DOM content loaded');

    var dashboardData = JSON.parse(document.getElementById('dashboard-data').textContent);

    var totalProjects = dashboardData.totalProjects;
    var totalTasks = dashboardData.totalTasks;
    var totalCompanies = dashboardData.totalCompanies;
    var activeProjects = dashboardData.activeProjects;
    var completedProjectsYtd = dashboardData.completedProjectsYtd;
    var tasks = dashboardData.tasks;
    var projectName = dashboardData.projectName;
    var companiesByState = dashboardData.companiesByState;

    console.log('Data:', totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState);

    initializeCharts(companiesByState, tasks);

    document.getElementById('project-select').addEventListener('change', function () {
        const projectId = this.value;
        console.log(`Selected project ID: ${projectId}`);

        const csrfToken = document.querySelector('[name=csrfmiddlewaretoken]').value;

        fetch(`/dashboard/?project_id=${projectId}`, {
            method: 'GET',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRFToken': csrfToken
            }
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('Data received:', data);
            updateDashboardData(
                data.total_projects,
                data.total_tasks,
                data.total_companies,
                data.active_projects,
                data.completed_projects_ytd,
                data.task_data,
                data.project_name,
                data.companies_by_state
            );
        })
        .catch(error => console.error('Error:', error));
    });
});

