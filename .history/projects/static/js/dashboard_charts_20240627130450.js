console.log('dashboard_charts.js loaded');

var pieChart;

function initializeCharts(companiesByState, tasks) {
    console.log('Initializing charts with:', { companiesByState, tasks });

    if (pieChart) {
        pieChart.destroy();
    }

    // Initialize Pie Chart
    var companiesCtx = document.getElementById('myPieChart').getContext('2d');
    var companiesLabels = companiesByState.map(data => data.state);
    var companiesData = companiesByState.map(data => data.count);
    var totalCompanies = companiesData.reduce((a, b) => a + b, 0);
    var companiesPercentages = companiesData.map(count => ((count / totalCompanies) * 100).toFixed(2));

    pieChart = new Chart(companiesCtx, {
        type: 'pie',
        data: {
            labels: companiesLabels.map((label, index) => `${label} (${companiesPercentages[index]}%)`),
            datasets: [{
                data: companiesPercentages,
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(199, 199, 199, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(199, 199, 199, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(context) {
                            var label = context.label || '';
                            var value = context.raw || 0;
                            return `${label}: ${value}%`;
                        }
                    }
                }
            }
        }
    });

    // Initialize Gantt Chart using Plotly
    var ganttData = tasks.map(task => ({
        x: [task.start, task.end],
        y: [task.name],
        type: 'bar',
        orientation: 'h',
        marker: {
            color: 'rgba(58, 71, 80, 0.6)',
            line: {
                color: 'rgba(58, 71, 80, 1.0)',
                width: 1
            }
        },
        text: task.progress + '%',
        hoverinfo: 'x+text'
    }));

    var ganttLayout = {
        title: 'Task Progress',
        barmode: 'stack',
        margin: {
            l: 150,
            r: 50,
            t: 50,
            b: 50
        },
        xaxis: {
            type: 'date'
        },
        showlegend: false
    };

    Plotly.newPlot('myGanttChart', ganttData, ganttLayout);

    updateTaskList(tasks);
    updateProjectName(tasks[0]?.project || 'All Projects');
}

function updateDashboardData(totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState) {
    console.log('Updating dashboard data with:', { totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState });

    document.getElementById('total-projects').textContent = totalProjects;
    document.getElementById('total-tasks').textContent = totalTasks;
    document.getElementById('total-companies').textContent = totalCompanies;
    document.getElementById('active-projects').textContent = activeProjects;
    document.getElementById('completed-projects-ytd').textContent = completedProjectsYtd;

    initializeCharts(companiesByState, tasks);
}

function updateTaskList(tasks) {
    console.log('Updating task list:', tasks);
    var taskList = document.getElementById('task-list');
    taskList.innerHTML = '';
    tasks.forEach(task => {
        var listItem = document.createElement('li');
        listItem.textContent = `${task.name}: ${task.start} to ${task.end} (Progress: ${task.progress}%)`;
        taskList.appendChild(listItem);
    });
}

function updateProjectName(projectName) {
    console.log('Updating project name to:', projectName);
    document.getElementById('project-name').textContent = projectName;
}

document.addEventListener('DOMContentLoaded', function () {
    console.log('DOM content loaded');

    var dashboardData = JSON.parse(document.getElementById('dashboard-data').textContent);

    var totalProjects = dashboardData.totalProjects;
    var totalTasks = dashboardData.totalTasks;
    var totalCompanies = dashboardData.totalCompanies;
    var activeProjects = dashboardData.activeProjects;
    var completedProjectsYtd = dashboardData.completedProjectsYtd;
    var tasks = dashboardData.tasks;
    var projectName = dashboardData.projectName;
    var companiesByState = dashboardData.companiesByState;

    console.log('Data:', totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks, projectName, companiesByState);

    initializeCharts(companiesByState, tasks);

    document.getElementById('project-select').addEventListener('change', function () {
        const projectId = this.value;
        console.log(`Selected project ID: ${projectId}`);

        const csrfToken = document.querySelector('[name=csrfmiddlewaretoken]').value;

        fetch(`/dashboard/?project_id=${projectId}`, {
            method: 'GET',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRFToken': csrfToken
            }
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('Data received:', data);
            updateDashboardData(
                data.total_projects,
                data.total_tasks,
                data.total_companies,
                data.active_projects,
                data.completed_projects_ytd,
                data.task_data,
                data.project_name,
                data.companies_by_state
            );
        })
        .catch(error => console.error('Error:', error));
    });
});

