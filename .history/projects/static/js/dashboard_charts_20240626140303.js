document.addEventListener('DOMContentLoaded', function () {
    function updateDashboardData(totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd, tasks) {
        // Pie chart data
        var pieData = {
            labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
            datasets: [{
                data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(153, 102, 255, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)'
                ],
                borderWidth: 1
            }]
        };

        // Bar chart data
        var barData = {
            labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
            datasets: [{
                label: 'Count',
                data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(153, 102, 255, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)'
                ],
                borderWidth: 1
            }]
        };

        // Line chart data
        var lineData = {
            labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
            datasets: [{
                label: 'Count',
                data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                fill: false,
                borderWidth: 1
            }]
        };

        // Update charts
        updateChart(pieChart, pieData);
        updateChart(barChart, barData);
        updateChart(lineChart, lineData);

        // Update task list with progress
        var taskList = document.getElementById('task-list');
        taskList.innerHTML = '';
        tasks.forEach(task => {
            var listItem = document.createElement('li');
            listItem.textContent = `${task.name}: ${task.start} to ${task.end} (Progress: ${task.progress}%)`;
            taskList.appendChild(listItem);
        });
    }

    function updateChart(chart, data) {
        chart.data = data;
        chart.update();
    }

    var totalProjects = {{ total_projects }};
    var totalTasks = {{ total_tasks }};
    var totalCompanies = {{ total_companies }};
    var activeProjects = {{ active_projects }};
    var completedProjectsYtd = {{ completed_projects_ytd }};
    var tasks = {{ tasks|safe }};

    var ctxPie = document.getElementById('myPieChart').getContext('2d');
    var pieChart = new Chart(ctxPie, {
        type: 'pie',
        data: {
            labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
            datasets: [{
                data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(153, 102, 255, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
        }
    });

    var ctxBar = document.getElementById('myBarChart').getContext('2d');
    var barChart = new Chart(ctxBar, {
        type: 'bar',
        data: {
            labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
            datasets: [{
                label: 'Count',
                data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(153, 102, 255, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    var ctxLine = document.getElementById('myLineChart').getContext('2d');
    var lineChart = new Chart(ctxLine, {
        type: 'line',
        data: {
            labels: ['Projects', 'Tasks', 'Companies', 'Active Projects', 'Completed Projects YTD'],
            datasets: [{
                label: 'Count',
                data: [totalProjects, totalTasks, totalCompanies, activeProjects, completedProjectsYtd],
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                fill: false,
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    document.getElementById('project-select').addEventListener('change', function () {
        const projectId = this.value;
        fetch(`?project_id=${projectId}`)
            .then(response => response.json())
            .then(data => {
                updateDashboardData(
                    data.total_projects,
                    data.total_tasks,
                    data.total_companies,
                    data.active_projects,
                    data.completed_projects_ytd,
                    data.task_data
                );
            });
    });
});
