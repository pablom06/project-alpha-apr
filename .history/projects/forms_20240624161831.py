from django.forms import ModelForm
from .models import Project, Company

class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]

class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = ["name", "company_name", "address:city", "address:state", "address:zip", "address:street", "address:country"]
