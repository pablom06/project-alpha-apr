from django.forms import ModelForm
from .models import Project, Company

class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]

class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = ["name", "street_address", "address: city", "address:state", "address:zip", "address:street", "address:country"]

    street_address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    zip_code = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
