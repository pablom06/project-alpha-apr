from django.shortcuts import render, get_object_or_404, redirect
from .models import Project

def show_projects(request):
    return render(request, "projects.html")

def show_projects(request, id):
    project_list = get_object_or_404(Project, id=id)
    project_items = todo_list.items.all()
    return render(request, 'projects.html', {'todo_list': todo_list, 'todo_items': todo_items})
