from django.forms import ModelForm, Select, ValidationError
from .models import Project, Company
from django import forms

class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner", "company"]
        widgets = {
            'company': Select(choices=[('', 'Select a Company')] + [(company.id, company.name) for company in Company.objects.all()]+ [('new', 'Add Company Not Listed')])
        }
    def clean(self):
        cleaned_data = super().clean()
        company = cleaned_data.get("company")
        new_company = cleaned_data.get("new_company")

        if company == 'new' and not new_company:
            raise ValidationError("You must add a new company name.")

        return cleaned_data
class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = ["name", "street_address", "city", "state", "zip_code", "description", "owner"]


To add a button for importing a file on the Companies page, you will need to implement a file upload form and a view to handle the file upload and processing. Here’s a step-by-step guide:

Step 1: Create a Form for File Upload
Create a form for uploading the CSV file.

projects/forms.py

python
Copy code
from django import forms

class UploadFileForm(forms.Form):
    file = forms.FileField(label='Select a CSV file')
