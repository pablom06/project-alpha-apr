from django.forms import ModelForm, Select, ValidationError
from .models import Project, Company
from django import forms

class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner", "company", "status"]
        widgets = {
            'company': Select(choices=[('', 'Select a Company')] + [(company.id, company.name) for company in Company.objects.all()]+ [('new', 'Add Company Not Listed')]),'status': Select(choices=[
                ('pending', 'Pending'),
                ('in_progress', 'In Progress'),
                ('completed', 'Completed')
            ])
        }
    def clean(self):
        cleaned_data = super().clean()
        company = cleaned_data.get("company")
        new_company = cleaned_data.get("new_company")

        if company == 'new' and not new_company:
            raise ValidationError("You must add a new company name.")

        return cleaned_data

class CompanyForm(ModelForm):
    STATE_CHOICES = [
        ('AL', 'Alabama'), ('AK', 'Alaska'), ('AZ', 'Arizona'), ('AR', 'Arkansas'),
        ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'),
        ('FL', 'Florida'), ('GA', 'Georgia'), ('HI', 'Hawaii'), ('ID', 'Idaho'),
        ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'),
        ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'),
        ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'),
        ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'),
        ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'),
        ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('OH', 'Ohio'), ('OK', 'Oklahoma'),
        ('OR', 'Oregon'), ('PA', 'Pennsylvania'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'),
        ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'),
        ('VT', 'Vermont'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'),
        ('WI', 'Wisconsin'), ('WY', 'Wyoming'),
    ]

    state = forms.ChoiceField(choices=STATE_CHOICES, required=True)
    class Meta:
        model = Company
        fields = ["name", "street_address", "city", "state", "zip_code", "description", "owner"]

class UploadFileForm(forms.Form):
    file = forms.FileField(label='Select a CSV file')
