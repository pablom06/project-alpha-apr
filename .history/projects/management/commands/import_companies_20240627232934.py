import csv
from django.core.management.base import BaseCommand
from projects.models import Company
from projects.forms import CompanyForm

class Command(BaseCommand):
    help = 'Import companies from a CSV file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The path to the CSV file')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']

        # Get the form fields
        form_fields = CompanyForm().fields.keys()

        with open(file_path, newline='', encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                company_data = {field: row.get(field) for field in form_fields if row.get(field) is not None}

                # Handle owner separately if it's provided as an ID in the CSV
                owner_id = row.get('owner_id')
                if owner_id:
                    company_data['owner_id'] = owner_id
                    
                if 'name' in company_data and company_data['name']:


                # Create or update the company
                Company.objects.update_or_create(name=company_data['name'], defaults=company_data)

        self.stdout.write(self.style.SUCCESS('Successfully imported companies'))
