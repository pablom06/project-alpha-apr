import csv
from django.core.management.base import BaseCommand
from projects.models import Company

class Command(BaseCommand):
    help = 'Import companies from a CSV file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The path to the CSV file')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']

        with open(file_path, newline='', encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                company_data = {
                    'name': row.get('name'),
                    'street_address': row.get('street_address'),
                    'city': row.get('city'),
                    'state': row.get('state'),
                    'country': row.get('country'),
                    'zip_code': row.get('zip_code'),
                    'description': row.get('description'),
                    'owner_id': row.get('owner_id')
                }

                # Filter out None values to avoid overwriting with None
                company_data = {key: value for key, value in company_data.items() if value is not None}

                # Create or update the company
                Company.objects.update_or_create(name=company_data['name'], defaults=company_data)

        self.stdout.write(self.style.SUCCESS('Successfully imported companies'))

