import csv
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from projects.models import Company
from projects.forms import CompanyForm

class Command(BaseCommand):
    help = 'Import companies from a CSV file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The path to the CSV file')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']

        # Get the form fields
        form_fields = CompanyForm().fields.keys()

        with open(file_path, newline='', encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                # Normalize keys to match form fields case insensitively
                normalized_row = {key.lower(): value for key, value in row.items()}
                company_data = {field: normalized_row.get(field.lower()) for field in form_fields if normalized_row.get(field.lower()) is not None}

                # Handle owner separately if it's provided as an ID in the CSV
                owner_id = normalized_row.get('owner_id')
                if owner_id:
                    try:
                        owner = User.objects.get(id=owner_id)
                        company_data['owner'] = owner
                    except User.DoesNotExist:
                        self.stdout.write(self.style.WARNING(f"User with ID {owner_id} does not exist. Skipping row: {row}"))
                        continue

                # Check if 'name' (or 'Name') field is present
                if 'name' in company_data and company_data['name']:
                    # Create or update the company
                    company, created = Company.objects.update_or_create(name=company_data['name'], defaults=company_data)
                    if created:
                        self.stdout.write(self.style.SUCCESS(f"Created company: {company.name}"))
                    else:
                        self.stdout.write(self.style.SUCCESS(f"Updated company: {company.name}"))
                else:
                    self.stdout.write(self.style.WARNING(f"Skipping row due to missing 'name': {row}"))

        self.stdout.write(self.style.SUCCESS('Successfully imported companies'))
