from django.shortcuts import render, get_object_or_404, redirect
from .models import Project

def list_projects(request):
    project_list = get_object_or_404(Project)
    project_items = project_list.items.all()
    return render(request, 'projects.html', {'project_list': project_list, 'project_items': project_items})
