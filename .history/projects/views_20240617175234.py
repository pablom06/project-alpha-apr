from django.shortcuts import render, get_object_or_404, redirect
from .models import Project

def list_projects(request):
    projects = Project.objects.all()
    context = {
        "projects": projects
    }
    return render(request, "projects/projects.html", {"projects": projects})
