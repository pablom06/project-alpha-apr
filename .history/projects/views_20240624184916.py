from django.shortcuts import render, get_object_or_404, redirect
from .models import Project, Company,Task
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm, CompanyForm

@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects.html", context)

@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    context = {"project": project, "tasks": tasks}
    return render(request, "projects/show_project.html", context)

@login_required
def new_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)

@login_required
def edit_project(request, id):
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect('show_project', id=project.id)
    else:
        form = ProjectForm(instance=project)
    context = {"form": form, "project": project}
    return render(request, "projects/edit_project.html", context)

@login_required
def list_companies(request):
    companies = Company.objects.filter(owner=request.user)
    context = {"companies": companies}
    return render(request, "projects/companies.html", context)

@login_required
def add_company(request):
    form = CompanyForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("list_companies")
    context = {"form": form}
    return render(request, "projects/add_company.html", context)

@login_required
def search_projects_by_name(request):
    query = request.GET.get("query")
    projects = Project.objects.filter(name__icontains=query, owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects.html", context)
@login_required
def create_task(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.project = project
            task.save()
            return redirect('show_project', id=project.id)
    else:
        form = TaskForm()
    context = {"form": form, "project": project}
    return render(request, "tasks/create_task.html", context)

@login_required
def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect('show_project', id=task.project.id)
    else:
        form = TaskForm(instance=task)
    context = {"form": form, "task": task}
    return render(request, "tasks/edit_task.html", context)
