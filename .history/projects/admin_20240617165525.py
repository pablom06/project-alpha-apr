from django.contrib import admin

class ProjectAdmin(admin.ModelAdmin):
    list_display = ("name", "owner", "description")

admin.site.register(Project, ProjectAdmin)
