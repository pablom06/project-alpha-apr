from django.urls import path
from .views import list_projects, show_

urlpatterns = [
    path("", list_projects, name = "list_projects"),
    path("int:id/", show_project, name = "list_projects")
]
