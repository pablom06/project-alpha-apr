from django.shortcuts import render, get_object_or_404, redirect
from .models import Project, Company
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm, CompanyForm, UploadFileForm
from tasks.forms import TaskForm
import csv

@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects.html", context)

@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    for task in tasks:
        print(f"Task: {task.name}, Company: {task.company.name if task.company else 'None'}")
    context = {"project": project, "tasks": tasks}
    return render(request, "projects/show_project.html", context)

@login_required
def new_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)

@login_required
def edit_project(request, id):
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect('show_project', id=project.id)
    else:
        form = ProjectForm(instance=project)
    context = {"form": form, "project": project}
    return render(request, "projects/edit_project.html", context)

@login_required
def search_projects_by_name(request):
    query = request.GET.get("query")
    projects = Project.objects.filter(name__icontains=query, owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects.html", context)

@login_required
def list_companies(request):
    companies = Company.objects.filter(owner=request.user)
    context = {"companies": companies}
    return render(request, "projects/companies.html", context)

@login_required
def add_company(request):
    form = CompanyForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("list_companies")
    context = {"form": form}
    return render(request, "projects/add_company.html", context)

@login_required
def edit_company(request, id):
    company = get_object_or_404(Company, id=id)
    if request.method == "POST":
        form = CompanyForm(request.POST, instance=company)
        if form.is_valid():
            form.save()
            return redirect('list_companies')
    else:
        form = rojectForm(instance=company)
    context = {"form": form, "company": company}
    return render(request, "projects/edit_company.html", context)

@login_required
def import_companies(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['file']
            reader = csv.DictReader(file.read().decode('utf-8').splitlines())

            form_fields = CompanyForm().fields.keys()
            for row in reader:
                company_data = {field: row.get(field) for field in form_fields if row.get(field) is not None}

                # Handle owner separately if it's provided as an ID in the CSV
                owner_id = row.get('owner_id')
                if owner_id:
                    company_data['owner_id'] = owner_id

                # Create or update the company
                Company.objects.update_or_create(name=company_data['name'], defaults=company_data)

            return redirect('list_companies')
    else:
        form = UploadFileForm()
    return render(request, 'projects/import_companies.html', {'form': form})
