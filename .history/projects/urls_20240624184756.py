from django.urls import path
from .views import list_projects, show_project, new_project, search_projects_by_name, list_companies, edit_project, add_company, edit_task

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("create/", new_project, name="create_project"),
    path("<int:id>/", show_project, name="show_project"),
    path("<int:id>/edit/", edit_project, name="edit_project"),
    path("search/", search_projects_by_name, name="search_projects"),
    path("companies/", list_companies, name="list_companies"),
    path("companies/add/", add_company, name="add_company"),
        path("tasks/create/<int:project_id>/", create_task, name="create_task"),
    path("tasks/edit/<int:id>/", edit_task, name="edit_task"),
]
