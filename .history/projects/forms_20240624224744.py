from django.forms import ModelForm, Select, ValidationError
from .models import Project, Company


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner", "company"]
        widgets = {
            'company': Select(choices=[('', 'Select a Company')] + [(company.id, company.name) for company in Company.objects.all()]+ [('new', 'Add Company Not Listed')])
        }
    def clean(self):
        cleaned_data = super().clean()
        company = cleaned_data.get("company")
        new_company = cleaned_data.get("new_company")

        if company == 'new' and not new_company:
            raise ValidationError("You must add a new company name.")

        return cleaned_data
class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = ["name", "street_address", "city", "state", "zip_code", "description", "owner"]
