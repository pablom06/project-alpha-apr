from django.shortcuts import render, get_object_or_404, redirect
from .models import Project, Company
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm

@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    context = {"project": project, "tasks": tasks}
    return render(request, "projects/show_project.html", context)


@login_required
def new_project(request):
    form = ProjectForm(request.POST or None)
    if form.is_valid():
        project = form.save(commit=False)
        project.owner = request.user
        project.save()
        return redirect("list_projects")
    context = {"form": form}
    return render(request, "projects/create_project.html", context)

@login_required
def list_companies(request):
    companies = Company.objects.filter(owner=request.user)
    context = {"companies": companies}
    return render(request, "projects/companies.html", context)

@
