from django.shortcuts import render, get_object_or_404, redirect

def show_projects(request):
    return render(request, "projects.html")

def show_projects(request, id):
    todo_list = get_object_or_404(Pr, id=id)
    todo_items = todo_list.items.all()
    return render(request, 'todos/todo_list_detail.html', {'todo_list': todo_list, 'todo_items': todo_items})
