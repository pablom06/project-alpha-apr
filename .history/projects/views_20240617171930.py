from django.shortcuts import render

def show_projects(request):
    return render(request, "projects.html")

def show_todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = todo_list.items.all()
    return render(request, 'todos/todo_list_detail.html', {'todo_list': todo_list, 'todo_items': todo_items})
