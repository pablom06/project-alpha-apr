from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects
    }
    return render(request, "projects/projects.html", context)

@login_required
def detail_project(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    context = {
        "project": project
    }
    return render(request, "projects/detail_project.html", context)
