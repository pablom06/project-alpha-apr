from django.forms import forms, model
from .models import Project, Company

class ProjectForm(forms.ModelForm):
    new_company = forms.CharField(max_length=200, required=False, label="Add New Company")
    class Meta:
        model = Project
        fields = ["name", "description", "owner", "company"]
        widgets = {
            'company': forms.Select(choices=[('', 'Select a Company')] + [(company.id, company.name) for company in Company.objects.all()]+ [('new', 'Add Company Not Listed')])
        }
    def clean(self):
        cleaned_data = super().clean()
        company = cleaned_data.get("company")
        new_company = cleaned_data.get("new_company")

        if company == 'new' and not new_company:
            raise forms.ValidationError("You must add a new company name.")

        return cleaned_data
class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ["name", "street_address", "city", "state", "zip_code", "description", "owner"]
