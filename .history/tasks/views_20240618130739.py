from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .forms import TaskForm

@login_required
def new_task(request):
    form = TaskForm(request.POST or None)
    if form.is_valid():
        task = form.save(commit=False)
        task.owner = request.user
        task.save()
        return redirect("list_projects")
    context = {
        "form": form
    }
    return render(request, "tasks/create_task.html", context)
