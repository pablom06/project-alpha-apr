from django.urls import path
from .views import new_task, list_tasks

urlpatterns = [
    path("create/", new_task, name = "create_task")
    path("mine/", list_tasks, name = "list_tasks")
]
