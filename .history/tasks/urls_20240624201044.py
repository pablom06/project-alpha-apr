from django.urls import path
from .views import new_task, list_tasks, create_task, edit_task, create_task_general

urlpatterns = [
    path("mine/", list_tasks, name="show_my_tasks"),
    path("create/<int:project_id>/", create_task, name="create_task"),
    path("create/", create_task_general, name="create_task_general"),
    path("edit/<int:id>/", edit_task, name="edit_task"),
]
