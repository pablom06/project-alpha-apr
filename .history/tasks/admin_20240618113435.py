from django.contrib import admin

class TaskAdmin(admin.ModelAdmin):
    list_display = ("name", "start_date", "due_date", "is_complete", "project", "assignee")

   name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_complete = models.BooleanField(default=False)
    project = models.ForeignKey(Project, related_name='tasks', on_delete=models.CASCADE)
    assignee
