from django import forms

class TaskForm(forms.Form):
    class meta:
        model = Task
        fields = ['name', 'description', 'status', 'project']
