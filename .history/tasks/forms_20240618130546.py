from django import ModelForm
from .models import Task

class TaskForm(ModelForm):
    class meta:
        model = Task
        exclude = ['is_complete']
        
