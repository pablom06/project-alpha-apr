from django.forms import ModelForm
from django import forms
from .models import Task

class TaskForm(ModelForm):
    class mMeta:
        model = Task
        exclude = ['is_complete']
