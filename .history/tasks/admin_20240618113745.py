from django.contrib import admin
from .models import Task

@ad
class TaskAdmin(admin.ModelAdmin):
    list_display = ("name", "start_date", "due_date", "is_complete", "project", "assignee")
