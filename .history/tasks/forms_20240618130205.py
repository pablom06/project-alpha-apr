from django import ModelForm
from .models import Task

class TaskForm(forms.Form):
    class meta:
        model = Task
        exclude = ['is_complete']
