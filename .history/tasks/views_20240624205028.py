from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm, Task
from projects.models import Project
from django.shortcuts import get_object_or_404
from .models import Task

@login_required
def new_task(request):
    form = TaskForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("list_projects")
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)

@login_required
def list_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/show_my_tasks.html", context)

@login_required
def create_task(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    if request.method == "POST":
        form = TaskForm(request.POST, project=project)
        if form.is_valid():
            task = form.save(commit=False)
            task.project = project
            if not task.company:
                task.company = project.company
            task.save()
            return redirect('show_project', id=project.id)
    else:
        form = TaskForm(initialproject=project)
    context = {"form": form, "project": project}
    return render(request, "tasks/create_task.html", context)

@login_required
def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect('show_project', id=task.project.id)
    else:
        form = TaskForm(instance=task)
    context = {"form": form, "task": task}
    return render(request, "tasks/edit_task.html", context)

@login_required
def create_task_general(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.assignee = request.user
            task.save()
            return redirect('show_my_tasks')
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)
