from django.contrib import admin

class TaskAdmin(admin.ModelAdmin):
    list_display = ("name", "owner", "description")
