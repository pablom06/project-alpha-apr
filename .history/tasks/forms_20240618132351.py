from django.forms import ModelForm
f
from .models import Task

class TaskForm(ModelForm):
    class meta:
        model = Task
        exclude = ['is_complete']
