from django.db import models
from django.contrib.auth.models import User
from projects.models import Project, Company
from datetime import datetime

lass Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_complete = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        User, related_name="tasks", on_delete=models.CASCADE, null=True
    )
    company = models.ForeignKey(Company, related_name="tasks", on_delete=models.CASCADE, null=True, blank=True)
    progress = models.IntegerField(default=0, editable=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.update_progress()
        super(Task, self).save(*args, **kwargs)

    def update_progress(self):
        if self.is_complete:
            self.progress = 100
        else:
            now = datetime.now()
            total_duration = (self.due_date - self.start_date).total_seconds()
            elapsed_duration = (now - self.start_date).total_seconds()
            remaining_duration = (self.due_date - now).total_seconds()

            if total_duration > 0:
                progress = 100 - (remaining_duration / total_duration) * 100
                self.progress = int(progress)
            else:
                self.progress = 0

            # If the task is past due, progress will be more than 100
            if now > self.due_date:
                overdue_duration = (now - self.due_date).total_seconds()
                overdue_percentage = (overdue_duration / total_duration) * 100
                self.progress = 100 + int(overdue_percentage)
