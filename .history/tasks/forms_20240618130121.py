from django import forms
from .models import Task

class TaskForm(forms.Form):
    class meta:
        model = Task
        exclude = ['is_complete']
