from django.urls import path
from .views import new_task, list_tasks, create_task, edit_task

urlpatterns = [
    path("create/", new_task, name="create_task"),
    path("mine/", list_tasks, name="show_my_tasks"),
    path("tasks/create/<int:project_id>/", create_task, name="create_task"),
    
    path("tasks/edit/<int:id>/", edit_task, name="edit_task"),
]
