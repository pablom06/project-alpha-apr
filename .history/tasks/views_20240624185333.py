from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm, Task

@login_required
def new_task(request):
    form = TaskForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("list_projects")
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)

@login_required
def list_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/show_my_tasks.html", context)
