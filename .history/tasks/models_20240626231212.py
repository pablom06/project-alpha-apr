from django.db import models
from django.contrib.auth.models import User
from projects.models import Project, Company
from datetime import datetime

class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_complete = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        User, related_name="tasks", on_delete=models.CASCADE, null=True
    )
    company = models.ForeignKey(Company, related_name="tasks", on_delete=models.CASCADE, null=True, blank=True)
    progress = models.IntegerField(default=0, editable=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.update_progress()
        super(Task, self).save(*args, **kwargs)

    def update_progress(self):
        if self.is_complete:
            self.progress = 100
        else:
            now = timezone.now()
            start_date = self.start_date
            due_date = self.due_date

            if start_date.tzinfo is None:
                start_date = timezone.make_aware(start_date, timezone.get_current_timezone())
            if due_date.tzinfo is None:
                due_date = timezone.make_aware(due_date, timezone.get_current_timezone())

            total_duration = (due_date - start_date).total_seconds()
            if total_duration <= 0:
                self.progress = 0
            else:
                elapsed_duration = (now - start_date).total_seconds()
                remaining_duration = (due_date - now).total_seconds()

                if remaining_duration <= 0:
                    overdue_duration = -remaining_duration
                    overdue_percentage = (overdue_duration / total_duration) * 100
                    self.progress = 100 + int(overdue_percentage)
                else:
                    self.progress = int((elapsed_duration / total_duration) * 100)
