from django.urls import path
from .views import new_task

urlpatterns = [
    path("create/", new_task, name = "create_task")
    
]
