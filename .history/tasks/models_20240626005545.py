from django.db import models
from django.contrib.auth.models import User
from projects.models import Project, Company
from datetime import datetime

class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_complete = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        "auth.User", related_name="tasks", on_delete=models.CASCADE, null=True
    )
    company = models.ForeignKey(Company, related_name="tasks", on_delete=models.CASCADE, null=True, blank=True)
    progress = models.IntegerField(default=0, editable=False)

    def __str__(self):
        return self.name

def save(self, *args, **kwargs):
        self.update_progress()
        super(Task, self).save(*args, **kwargs)

    def update_progress(self):
        if self.is_complete:
            self.progress = 100
        else:
            total_duration = (self.due_date - self.start_date).total_seconds()
            elapsed_duration = (datetime.now() - self.start_date).total_seconds()
            if total_duration > 0:
                self.progress = min(int((elapsed_duration / total_duration) * 100), 100)
            else:
                self.progress = 0
