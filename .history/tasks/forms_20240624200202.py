from django.forms import ModelForm
from .models import Task, Project
from projects.models import Company

class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner", "company"]

class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = ["name", "street_address", "city", "state", "zip_code", "description", "owner"]

class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ["name", "assignee", "start_date", "due_date", "company", "is_complete", "project]
