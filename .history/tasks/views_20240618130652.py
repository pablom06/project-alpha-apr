from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required
def list_projects(request):
    return render(request, "projects/projects.html")
