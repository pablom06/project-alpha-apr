from django import forms
from .models import Task

class TaskForm(forms.Form):
    class meta:
        model = Task
        exclude
        fields = ['name', 'start_date', 'due_date', 'is_complete', 'project', 'assignee']
