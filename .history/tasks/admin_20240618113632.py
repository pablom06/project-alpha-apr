from django.contrib import admin
f

class TaskAdmin(admin.ModelAdmin):
    list_display = ("name", "start_date", "due_date", "is_complete", "project", "assignee")
